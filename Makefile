obj-m	:= src/asus-rog-nb-wmi.o

KERNELDIR ?= /lib/modules/$(shell uname -r)/build
PWD       := $(shell pwd)

all: default

default:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

install:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules_install

clean:
	rm -rf src/*.o src/*~ src/.*.cmd src/*.ko src/*.mod.c \
		.tmp_versions modules.order Module.symvers

dkmsclean:
	@dkms remove asus-rog-nb-wmi/0.3.0 --all || true
	@dkms remove asus-rog-nb-wmi/1.0.0 --all || true

dkms: dkmsclean
	dkms add .
	dkms install -m asus-rog-nb-wmi -v 1.0.0

onboot:
	echo "blacklist asus-nb-wmi" > /etc/modprobe.d/asus-rog-nb-wmi.conf

noboot:
	rm -f /etc/modprobe/asus-rog-nb-wmi.conf
